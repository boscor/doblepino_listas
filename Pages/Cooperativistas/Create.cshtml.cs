﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using AccesoDatos;
using Modelo.Persona;

namespace Listas.Cooperativistas
{
    public class CreateModel : PageModel
    {
        private readonly AccesoDatos.MyContext _context;

        public CreateModel(AccesoDatos.MyContext context)
        {
            _context = context;
        }      

        public IActionResult OnGet()
        {
            ViewData["DireccionId"] = new SelectList(_context.Direcciones, "Id", "Calle");
            return Page();
        }

        [BindProperty]
        public Cooperativista Cooperativista { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Cooperativistas.Add(Cooperativista);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
