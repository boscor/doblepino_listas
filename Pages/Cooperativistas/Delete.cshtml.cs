﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AccesoDatos;
using Modelo.Persona;

namespace Listas.Cooperativistas
{
    public class DeleteModel : PageModel
    {
        private readonly AccesoDatos.MyContext _context;

        public DeleteModel(AccesoDatos.MyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Cooperativista Cooperativista { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Cooperativista = await _context.Cooperativistas
                .Include(c => c.Direccion).FirstOrDefaultAsync(m => m.Id == id);

            if (Cooperativista == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Cooperativista = await _context.Cooperativistas.FindAsync(id);

            if (Cooperativista != null)
            {
                _context.Cooperativistas.Remove(Cooperativista);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
