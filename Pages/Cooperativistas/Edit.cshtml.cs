﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AccesoDatos;
using Modelo.Persona;

namespace Listas.Cooperativistas
{
    public class EditModel : PageModel
    {
        private readonly AccesoDatos.MyContext _context;

        public EditModel(AccesoDatos.MyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Cooperativista Cooperativista { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Cooperativista = await _context.Cooperativistas
                .Include(c => c.Direccion).FirstOrDefaultAsync(m => m.Id == id);

            if (Cooperativista == null)
            {
                return NotFound();
            }
           ViewData["DireccionId"] = new SelectList(_context.Direcciones, "Id", "Calle");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Cooperativista).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CooperativistaExists(Cooperativista.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CooperativistaExists(int id)
        {
            return _context.Cooperativistas.Any(e => e.Id == id);
        }
    }
}
