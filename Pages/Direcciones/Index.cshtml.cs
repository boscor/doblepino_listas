﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AccesoDatos;
using Modelo.Persona;

namespace Listas.Direcciones
{
    public class IndexModel : PageModel
    {
        private readonly AccesoDatos.MyContext _context;

        public IndexModel(AccesoDatos.MyContext context)
        {
            _context = context;
        }

        public IList<Direccion> Direccion { get;set; }

        public async Task OnGetAsync()
        {
            Direccion = await _context.Direcciones.ToListAsync();
        }
    }
}
