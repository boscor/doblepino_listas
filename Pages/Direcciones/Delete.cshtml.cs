﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AccesoDatos;
using Modelo.Persona;

namespace Listas.Direcciones
{
    public class DeleteModel : PageModel
    {
        private readonly AccesoDatos.MyContext _context;

        public DeleteModel(AccesoDatos.MyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Direccion Direccion { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Direccion = await _context.Direcciones.FirstOrDefaultAsync(m => m.Id == id);

            if (Direccion == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Direccion = await _context.Direcciones.FindAsync(id);

            if (Direccion != null)
            {
                _context.Direcciones.Remove(Direccion);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
