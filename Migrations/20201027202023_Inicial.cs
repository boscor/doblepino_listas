﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Listas.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bitacoras",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Fecha = table.Column<DateTime>(nullable: false),
                    FechaCaptura = table.Column<DateTime>(nullable: false),
                    Observacion = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bitacoras", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Direcciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TelefonoFijo = table.Column<string>(nullable: true),
                    Calle = table.Column<string>(nullable: false),
                    NumeroExterior = table.Column<string>(nullable: false),
                    NumeroInterior = table.Column<string>(nullable: true),
                    Colonia = table.Column<string>(nullable: false),
                    Localidad = table.Column<string>(nullable: true),
                    Municipio = table.Column<string>(nullable: false),
                    Estado = table.Column<int>(nullable: false),
                    CP = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Direcciones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposResiduo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true),
                    PadreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposResiduo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TiposResiduo_TiposResiduo_PadreId",
                        column: x => x.PadreId,
                        principalTable: "TiposResiduo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnidadesMedida",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnidadesMedida", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonasFisicas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true),
                    ApellidoPaterno = table.Column<string>(nullable: false),
                    ApellidoMaterno = table.Column<string>(nullable: false),
                    CURP = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    TelefonoMovil = table.Column<string>(nullable: false),
                    DireccionId = table.Column<int>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    EsSocio = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonasFisicas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonasFisicas_Direcciones_DireccionId",
                        column: x => x.DireccionId,
                        principalTable: "Direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Residuos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true),
                    Clasificacion = table.Column<string>(nullable: false),
                    TipoResiduoId = table.Column<int>(nullable: false),
                    SeEnsucia = table.Column<bool>(nullable: false),
                    SeHumedece = table.Column<bool>(nullable: false),
                    SeEmpolva = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residuos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Residuos_TiposResiduo_TipoResiduoId",
                        column: x => x.TipoResiduoId,
                        principalTable: "TiposResiduo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Almacenes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true),
                    EncargadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Almacenes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Almacenes_PersonasFisicas_EncargadoId",
                        column: x => x.EncargadoId,
                        principalTable: "PersonasFisicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BitacoraCooperativista",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BitacoraId = table.Column<int>(nullable: false),
                    CooperativistaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BitacoraCooperativista", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BitacoraCooperativista_Bitacoras_BitacoraId",
                        column: x => x.BitacoraId,
                        principalTable: "Bitacoras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BitacoraCooperativista_PersonasFisicas_CooperativistaId",
                        column: x => x.CooperativistaId,
                        principalTable: "PersonasFisicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true),
                    NombreComercial = table.Column<string>(nullable: true),
                    RFC = table.Column<string>(maxLength: 13, nullable: true),
                    EsPersonaMoral = table.Column<bool>(nullable: false),
                    ContactoId = table.Column<int>(nullable: true),
                    DireccionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clientes_PersonasFisicas_ContactoId",
                        column: x => x.ContactoId,
                        principalTable: "PersonasFisicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Clientes_Direcciones_DireccionId",
                        column: x => x.DireccionId,
                        principalTable: "Direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BitacoraResiduo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BitacoraId = table.Column<int>(nullable: false),
                    Peso = table.Column<decimal>(nullable: false),
                    PesoProgramado = table.Column<decimal>(nullable: false),
                    MontoRecuperacion = table.Column<decimal>(nullable: false),
                    PorcentajeHumedad = table.Column<decimal>(nullable: false),
                    PorcentajePolvo = table.Column<decimal>(nullable: false),
                    PorcentajeSuciedad = table.Column<decimal>(nullable: false),
                    ResiduoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BitacoraResiduo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BitacoraResiduo_Bitacoras_BitacoraId",
                        column: x => x.BitacoraId,
                        principalTable: "Bitacoras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BitacoraResiduo_Residuos_ResiduoId",
                        column: x => x.ResiduoId,
                        principalTable: "Residuos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ResiduoAlmacen",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AlmacenId = table.Column<int>(nullable: false),
                    ResiduoId = table.Column<int>(nullable: false),
                    Existencia = table.Column<decimal>(nullable: false),
                    UnidadMedidaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResiduoAlmacen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResiduoAlmacen_Almacenes_AlmacenId",
                        column: x => x.AlmacenId,
                        principalTable: "Almacenes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResiduoAlmacen_Residuos_ResiduoId",
                        column: x => x.ResiduoId,
                        principalTable: "Residuos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResiduoAlmacen_UnidadesMedida_UnidadMedidaId",
                        column: x => x.UnidadMedidaId,
                        principalTable: "UnidadesMedida",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contratos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Observacion = table.Column<string>(maxLength: 500, nullable: true),
                    CostoEvento = table.Column<decimal>(nullable: false),
                    CostoEventoExtraordinario = table.Column<decimal>(nullable: false),
                    EventosporMes = table.Column<int>(nullable: false),
                    ClienteId = table.Column<int>(nullable: false),
                    DiasCredito = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contratos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contratos_Clientes_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Clientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Almacenes_EncargadoId",
                table: "Almacenes",
                column: "EncargadoId");

            migrationBuilder.CreateIndex(
                name: "IX_BitacoraCooperativista_BitacoraId",
                table: "BitacoraCooperativista",
                column: "BitacoraId");

            migrationBuilder.CreateIndex(
                name: "IX_BitacoraCooperativista_CooperativistaId",
                table: "BitacoraCooperativista",
                column: "CooperativistaId");

            migrationBuilder.CreateIndex(
                name: "IX_BitacoraResiduo_BitacoraId",
                table: "BitacoraResiduo",
                column: "BitacoraId");

            migrationBuilder.CreateIndex(
                name: "IX_BitacoraResiduo_ResiduoId",
                table: "BitacoraResiduo",
                column: "ResiduoId");

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_ContactoId",
                table: "Clientes",
                column: "ContactoId");

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_DireccionId",
                table: "Clientes",
                column: "DireccionId");

            migrationBuilder.CreateIndex(
                name: "IX_Contratos_ClienteId",
                table: "Contratos",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonasFisicas_DireccionId",
                table: "PersonasFisicas",
                column: "DireccionId");

            migrationBuilder.CreateIndex(
                name: "IX_ResiduoAlmacen_AlmacenId",
                table: "ResiduoAlmacen",
                column: "AlmacenId");

            migrationBuilder.CreateIndex(
                name: "IX_ResiduoAlmacen_ResiduoId",
                table: "ResiduoAlmacen",
                column: "ResiduoId");

            migrationBuilder.CreateIndex(
                name: "IX_ResiduoAlmacen_UnidadMedidaId",
                table: "ResiduoAlmacen",
                column: "UnidadMedidaId");

            migrationBuilder.CreateIndex(
                name: "IX_Residuos_TipoResiduoId",
                table: "Residuos",
                column: "TipoResiduoId");

            migrationBuilder.CreateIndex(
                name: "IX_TiposResiduo_PadreId",
                table: "TiposResiduo",
                column: "PadreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BitacoraCooperativista");

            migrationBuilder.DropTable(
                name: "BitacoraResiduo");

            migrationBuilder.DropTable(
                name: "Contratos");

            migrationBuilder.DropTable(
                name: "ResiduoAlmacen");

            migrationBuilder.DropTable(
                name: "Bitacoras");

            migrationBuilder.DropTable(
                name: "Clientes");

            migrationBuilder.DropTable(
                name: "Almacenes");

            migrationBuilder.DropTable(
                name: "Residuos");

            migrationBuilder.DropTable(
                name: "UnidadesMedida");

            migrationBuilder.DropTable(
                name: "PersonasFisicas");

            migrationBuilder.DropTable(
                name: "TiposResiduo");

            migrationBuilder.DropTable(
                name: "Direcciones");
        }
    }
}
