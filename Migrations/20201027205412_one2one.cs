﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Listas.Migrations
{
    public partial class one2one : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PersonasFisicas_DireccionId",
                table: "PersonasFisicas");

            migrationBuilder.DropIndex(
                name: "IX_Clientes_DireccionId",
                table: "Clientes");

            migrationBuilder.CreateIndex(
                name: "IX_PersonasFisicas_DireccionId",
                table: "PersonasFisicas",
                column: "DireccionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_DireccionId",
                table: "Clientes",
                column: "DireccionId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PersonasFisicas_DireccionId",
                table: "PersonasFisicas");

            migrationBuilder.DropIndex(
                name: "IX_Clientes_DireccionId",
                table: "Clientes");

            migrationBuilder.CreateIndex(
                name: "IX_PersonasFisicas_DireccionId",
                table: "PersonasFisicas",
                column: "DireccionId");

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_DireccionId",
                table: "Clientes",
                column: "DireccionId");
        }
    }
}
